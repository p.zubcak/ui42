<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="{{ asset(mix('css/app.css')) }}">

    <title>UI42</title>
</head>
<body>
    <header>
        <div class="container pt-2">
            <div class="row">
                <div class="col ps-0">
                    <a href='{{ url("/") }}'><img src="{{ asset('images/logo.png') }}" alt="ui42"></a>
                </div>
                <div class="col d-flex align-items-center justify-content-end">
                    <div>
                        <a href="#" class="text-decoration-none fw-bold">Kontakty a čísla na oddelenie</a>
                    </div>
                    <select class="form-select w-auto border-0 text-black-50" aria-label="Select language">
                        <option selected>EN</option>
                        <option value="1">SK</option>
                        <option value="2">DE</option>
                    </select>
                    <input type="text" class="form-control page-search-input me-2">
                    <button class="btn login-btn text-white">Prihlásenie</button>
                </div>
            </div>
            <div class="row pt-2 pb-2">
                <ul class="nav">
                    <li class="nav-item"><a href="#" class="nav-link">O nás</a></li>
                    <li class="nav-item"><a href="#" class="nav-link">Zoznam miest</a></li>
                    <li class="nav-item"><a href="#" class="nav-link">Inšpekcia</a></li>
                    <li class="nav-item"><a href="#" class="nav-link">Kontakt</a></li>
                </ul>
            </div>
        </div>
    </header>
    <main>
        @yield('content')
    </main>
    <footer>
        <div class="container pt-5 pb-5">
            <div class="row">
                <div class="col-3">

                    <div class="footer-box">
                        <h6>Adresa a kontakt</h6>
                        ŠÚKL<br>
                        Kvetná 11<br>
                        825 08 Bratislava 26<br>
                        Ústredňa:<br>
                        +421-2-50701 111
                    </div>

                    <div class="footer-box">
                        <h6>Kontakty</h6>
                        <ul class="list-unstyled">
                            <li><a href="">telefónne čísla</a></li>
                            <li><a href="">adresa</a></li>
                            <li><a href="">úradné hodiny</a></li>
                            <li><a href="">bankové spustenie</a></li>
                        </ul>
                    </div>

                    <div class="footer-box">
                        <h6>Informácie pre verejnosť</h6>
                        <ul class="list-unstyled">
                            <li><a href="">Zoznam vyvezených liekov</a></li>
                            <li><a href="">MZ SR</a></li>
                            <li><a href="">Národný portál zdravia</a></li>
                        </ul>
                    </div>

                </div>

                <div class="col-3">
                    <div class="footer-box">
                        <h6>O nás</h6>
                        <ul class="list-unstyled">
                            <li><a href="">Dotazníky</a></li>
                            <li><a href="">Hlavný predstavitelia</a></li>
                            <li><a href="">Základné dokumenty</a></li>
                            <li><a href="">Zmluvy za ŠÚKL</a></li>
                            <li><a href="">História a súčasnosť</a></li>
                            <li><a href="">Národná spolupráca</a></li>
                            <li><a href="">Medzinárodná spolupráca</a></li>
                            <li><a href="">Poradné orgány</a></li>
                            <li><a href="">Legislatíva</a></li>
                            <li><a href="">Priestupky a iné správne delikty</a></li>
                            <li><a href="">Zoznam dlžníkov</a></li>
                            <li><a href="">Sadzobník ŠÚKL</a></li>
                            <li><a href="">Verejné obstarávanie</a></li>
                            <li><a href="">Vzdelávacie akcie a prezentácie</a></li>
                            <li><a href="">Konzultácie</a></li>
                            <li><a href="">Voľné pracovné miesta (0)</a></li>
                            <li><a href="">Poskytovanie informácií</a></li>
                            <li><a href="">Sťažnosti a petície</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-3">

                    <div class="footer-box">
                        <h6>Médiá</h6>
                        <ul class="list-unstyled">
                            <li><a href="">Tlačové správy</a></li>
                            <li><a href="">Lieky v médiách</a></li>
                            <li><a href="">Kontakt pre médiá</a></li>
                        </ul>
                    </div>

                    <div class="footer-box">
                        <h6>Databázy a servis</h6>
                        <ul class="list-unstyled">
                            <li><a href="">Databáza liekov a zdravotníckych pomôcok</a></li>
                            <li><a href="">Iné zoznamy</a></li>
                            <li><a href="">Kontaktný formulár</a></li>
                            <li><a href="">Mapa stránok</a></li>
                            <li><a href="">A-Z index</a></li>
                            <li><a href="">Linky</a></li>
                            <li><a href="">RSS</a></li>
                            <li><a href="">Doplnok pre internetový prehliadač</a></li>
                            <li><a href="">Prehliadače formátov</a></li>
                        </ul>
                    </div>

                </div>

                <div class="col-3">
                    <div class="footer-box">
                        <h6>Drogové prekurzory</h6>
                        <ul class="list-unstyled">
                            <li><a href="">Aktuality</a></li>
                            <li><a href="">Legislatíva</a></li>
                            <li><a href="">Pokyny</a></li>
                            <li><a href="">Kontakt</a></li>
                        </ul>
                    </div>

                    <div class="footer-box">
                        <h6>Iné</h6>
                        <ul class="list-unstyled">
                            <li><a href="">Linky</a></li>
                            <li><a href="">Mapa stránok</a></li>
                            <li><a href="">FAQ</a></li>
                            <li><a href="">Podmienky používania</a></li>
                        </ul>
                    </div>

                    <div class="footer-box rapid">
                        <h6>Rapid alert system</h6>
                        <ul class="list-unstyled">
                            <li><a href="">Rýchla výstraha vyplývajúca z nedostatkov v kvalite liekov</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <script src="{{ asset(mix('js/app.js')) }}"></script>
</body>
</html>