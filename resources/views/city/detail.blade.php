
@extends('layouts.app')

@section('content')
    <div class="container mt-5 mb-5">

        <div class="row">
            <div class="col d-flex justify-content-start align-items-center flex-column">
                <div class="display-5">
                    Detail obce
                </div>

                <div class="w-100 row city-detail-box mt-4 shadow">
                    <div class="col-6 left d-flex flex-column justify-content-center align-items-center">

                        <table>
                            <tr>
                                <td>Meno starostu:</td>
                                <td>{{ $city->mayor_name }}</td>
                            </tr>
                            <tr>
                                <td>Adresa obecného úradu:</td>
                                <td>{{ $city->city_hall_address }}</td>
                            </tr>
                            <tr>
                                <td>Telefón:</td>
                                <td>{{ $city->phone }}</td>
                            </tr>
                            <tr>
                                <td>Fax:</td>
                                <td>{{ $city->fax }}</td>
                            </tr>
                            <tr>
                                <td>Email:</td>
                                <td>
                                    @if (trim($city->email)!="")
                                        <a href="mailto:{{ $city->email }}">{{ $city->email }}</a>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>Web:</td>
                                <td>
                                    @if (trim($city->web)!="")
                                        <a href="https://{{ $city->web }}" target="_blank">{{ $city->web }}</a>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>Zemepisné súradnice:</td>
                                <td>{{ $city->latitude }}, {{ $city->longitude }}</td>
                            </tr>
                        </table>

                    </div>
                    <div class="col-6 right d-flex justify-content-center align-items-center flex-column" >
                        <img src="{{ asset($city->image_url) }}">
                        <div class="h1 text-primary fw-bold mt-4">{{ $city->name }}</div>
                    </div>
                </div>

            </div>
        </div>

    </div>
@endsection