
<div class="container pt-5 pb-5 d-flex flex-column justify-content-start align-items-center">
    <div class="h2">Výsledky vyhľadávania</div>
    <div class="d-flex flex-wrap">
        @if (count($cities)>0)
            @foreach ( $cities as $city)
                <a class="d-block m-2 bg-light rounded px-4 py-2 fw-bold text-black text-decoration-none city-list-item" href="{{ url('/city/' . $city->id) }}">{{ $city->name }}</a>
            @endforeach
        @else
            <div>Nenašlo sa žiadne mesto</div>
        @endif
    </div>
</div>