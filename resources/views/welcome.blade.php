
@extends('layouts.app')

@section('content')
    <div class="homepage-search-bar container-fluid d-flex justify-content-center align-items-center flex-column">
        <div class="display-3 fw-light text-white">Vyhľadať v databáze obcí</div>
        <input id="city-search-input" type="text" class="mt-4 form-control city-search-input" placeholder="Zadajte názov">
    </div>

    <div id="city-search-results">

    </div>

    <script>
        window.addEventListener('DOMContentLoaded', (event) => {
            document.getElementById('city-search-input').addEventListener("keyup", _.debounce(searchCity, 400))
        })

        function searchCity() {

            let searchString = document.getElementById('city-search-input').value
            if (searchString != "") {
                axios.get('{{ url("/cities") }}/' + encodeURIComponent(searchString) )
                .then(function (response) {
                    console.log(response);
                    document.getElementById('city-search-results').innerHTML = response.data
                })
                .catch(function (error) {
                    console.log(error);
                })
            } else {
                document.getElementById('city-search-results').innerHTML = ''
            }

        }
    </script>
@endsection