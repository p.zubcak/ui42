<?php

namespace App\Console\Commands;

use App\Imports\ImportGeocode;
use Illuminate\Console\Command;

class DataGeocodeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:geocode';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command geolocate all cities based on their address';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $import = new ImportGeocode();
        $import->import();

        foreach ($import->log as $logMsg) {
            $this->info($logMsg);
        }

        return Command::SUCCESS;
    }
}
