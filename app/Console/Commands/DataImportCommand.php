<?php

namespace App\Console\Commands;

use App\Imports\ImportCities;
use Illuminate\Console\Command;

class DataImportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import cities from web for NR district';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $import = new ImportCities();
        $import->import();

        foreach ($import->log as $logMsg) {
            $this->info($logMsg);
        }


        return Command::SUCCESS;
    }
}
