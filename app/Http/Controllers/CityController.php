<?php

namespace App\Http\Controllers;

use App\Models\City;
use Illuminate\Http\Request;

class CityController extends Controller
{

    /**
     * Akcia v pripade ajaxoveho volania vrati zoznam miest,
     * odfiltrovany podla hladaneho vyrazu a zoradeny podla name
     * V pripade ze nie je volana ajaxom spravi redirect na homepage
     *
     * @param string $searchString - hladany retazec
     * @return void
     */
    public function index(string $searchString="")
    {
        if(request()->ajax()){
            $cities = City::where('name','like','%' . $searchString . '%')->orderBy('name')->get();

            return view('city.list', ['cities' => $cities]);
        } else {
            return redirect('/');
        }
    }

    /**
     * Akcia zobrazi detail mesta
     *
     * @param City $city
     * @return void
     */
    public function show(City $city)
    {
        return view('city.detail', ['city' => $city]);
    }
}
