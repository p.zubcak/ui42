<?php

namespace App\Imports;

use App\Models\City;
use Illuminate\Support\Facades\Http;


class ImportGeocode
{

    const GOOGLE_GEOCODE_API_URL = 'https://maps.googleapis.com/maps/api/geocode';

    public $log = [];

    /**
     * Create a new ImportCities instance.
     *
     * @param string $districtUrl
     */
    public function __construct()
    {

    }

    /**
     * Funkcia import vytiahne z db z tabulky cities vsetky mesta ktore maju latitude alebo longitude nezadane
     * ak maju tieto mesta zadanu adresu pokusi sa cez google geocode api zistit ich suradnice, ak sa to podari zapise suradnice do db
     *
     * @return void
     */
    public function import(): void
    {

        $cities = City::where('latitude', NULL)->whereOr('longitude', NULL)->get();

        foreach ($cities as $city) {

            if (trim($city->city_hall_address) != '') {

                $url =
                    self::GOOGLE_GEOCODE_API_URL .
                    '/json?address=' .
                    urlencode($city->city_hall_address) .
                    '&key=' .
                    env('GOOGLE_API_KEY');

                $response = Http::get($url);

                if ($response->successful()) {
                    $result = $response->json();
                    if ($result['status'] == 'OK') {

                        if ( is_array($result['results']) && count($result['results']) > 0 ) {

                            $city->latitude = $result['results'][0]['geometry']['location']['lat'];
                            $city->longitude = $result['results'][0]['geometry']['location']['lng'];

                            $city->save();

                        } else {
                            $this->log[] = "GEOCODE no results for city: " . $city->name;
                        }
                    }
                } else {
                    $this->log[] = "GEOCODE request failed for city: " . $city->name;
                }
            }

        }

    }

}