<?php

namespace App\Imports;

use DOMXPath;
use DOMDocument;
use App\Models\City;
use App\Models\District;
use Exception;
use Illuminate\Support\Facades\Validator;

class ImportCities
{
    private $districtsListUrl = "https://www.e-obce.sk/kraj/NR.html";

    public $log = [];

    /**
     * Create a new ImportCities instance.
     *
     */
    public function __construct()
    {

    }

    /**
     * funkcia na spustenie importu
     *
     * @return void
     */
    public function import(): void
    {
        $this->parseDistrictsList($this->districtsListUrl);
    }

    /**
     * funkcia stiahne web stranku na ktorej sa nachadza zoznam okresov,
     * na tejto stranke sa vyhladaju okresy a ich url.
     * Kazdy najdeny okres sa ulozi do db ak este v nej nieje,
     * to ci uz v db je sa porovnava na zaklade url okresu.
     * Po ulozeni do db sa zavola funkcia na spracovanie detailu okresu parseDistrictDetail
     *
     * @param string $districtsListUrl - url stranky na ktorej sa nachadza zoznam okresov
     * @return void
     */
    private function parseDistrictsList($districtsListUrl): void
    {

        $elements = [];

        try {
            $xpath = $this->getXpathDoc($districtsListUrl);

            $elements = $xpath->query('//table/tr/td/b[text()="OKRES:"]/following-sibling::a');

        } catch(Exception $e) {
            $this->log[] = $e->getMessage();
        }

        foreach ($elements as $element) {
            $districtDetailurl = "";
            if ( $element->attributes != NULL && $element->attributes->getNamedItem("href") != NULL ) {
                $districtDetailurl = $element->attributes->getNamedItem("href")->textContent;
            }

            $districtData = [
                'name'          => $element->textContent,
                'import_url'    => $districtDetailurl,
            ];

            $validator = Validator::make($districtData, [
                'name'          => 'required',
                'import_url'    => 'required|url',
            ]);

            if (!$validator->fails()) {

                $districtModel = District::firstOrCreate(
                    ['import_url' => $districtData['import_url']],
                    ['name' => $districtData['name'], 'import_url' => $districtData['import_url'] ]
                );

                try {
                    $this->parseDistrictDetail($districtDetailurl, $districtModel->id);
                } catch(Exception $e) {
                    $this->log[] = $e->getMessage();
                }

            } else {
                $this->log[] = "INVALID DISTRICT DATA: name: " . json_encode($districtData);
            }

        }

    }

    /**
     * Funkcia dostane ako parameter url stranky na ktorej sa nachadza zoznam miest,
     * pomocou xpath sa najdu vsetky mesta a ich url.
     * Pre kazde najdene mesto ak je platna url sa zavola funkcia parseCityDetail,
     * ktorej sa odovzda url mesta a tato funkcia vrati detaily o meste ktore najde na stranke zodpovedajucej danej url.
     * Nasledne sa ziskane udaje o meste ulozia do db, ak mesto v db neexistuje vlozi sa novy zaznam
     *
     * @param string $districtDetailurl - url stranky na ktorej sa nachadza detail konkretneho okresu, na tejto stranke sa nachadza zoznam miest v danom okrese
     * @param integer $districtId - id okresu z db, ide o okres ktoreho url sa ide spracovavat
     * @return void
     */
    private function parseDistrictDetail(string $districtDetailurl, int $districtId): void
    {

        $cities = [];
        $elements = [];

        try {
            $xpath = $this->getXpathDoc($districtDetailurl);
            $elements = $xpath->query('//table/tr/td/b[contains(text(),"Vyberte si obec alebo mesto z okresu")]/following-sibling::table[1]/tr/td/a');
        } catch(Exception $e) {
            $this->log[] = $e->getMessage();
        }

        foreach ($elements as $element) {

            $cityDetailUrl = "";
            if ( $element->attributes != NULL && $element->attributes->getNamedItem("href") != NULL ) {
                $cityDetailUrl = $element->attributes->getNamedItem("href")->textContent;
            }

            $cityData = [
                'name'          => $element->textContent,
                'import_url'    => $cityDetailUrl,
                'district_id'   => $districtId,
            ];

            try {

                $cityDetails = $this->parseCityDetail($cityDetailUrl);
                $cityData = array_merge($cityData, $cityDetails);

                $validator = Validator::make($cityData, [
                    'name'          => 'required',
                    'import_url'    => 'required|url',
                ]);

                if ($validator->fails()) {
                    $this->log[] = "INVALID CITY DATA: " . json_encode($cityData);
                } else {
                    $cities[] = $cityData;
                }

            } catch(Exception $e) {
                $this->log[] = $e->getMessage() . " CITY DATA : " . json_encode($cityData);
            }

        }

        if (count($cities) > 0) {
            City::upsert($cities, "import_url", []);
        }

    }

    /**
     *  Funkcia spracuje stranku s detailom emsta najde v nej vsetky pozadovane udaje:
     *  phone, fax, email, web, mayor_name, city_hall_address
     *  tieto udaje vrati vo forme asociativneho pola.
     *  Zaroven ak najde url obrazka , staihne tento obrazok do public adresara public/images/cities,
     *  ak uz obrazok s rovnakym menom existuje tak ho nestahuje
     *
     * @param string $cityDetailUrl - url stranky s detailom mesta
     * @return array - vracia asociativne pole s detailmi o meste
     */
    private function parseCityDetail(string $cityDetailUrl): array
    {
        $cityDetailData = [];

        try {
            $xpath = $this->getXpathDoc($cityDetailUrl);

            $mapDataXpath = [
                'phone'         => '//div[@class="adbox"]/following-sibling::table[1]/tr[3]/td[4]/table/tr/td',
                'fax'           => '//div[@class="adbox"]/following-sibling::table[1]/tr[4]/td[3]',
                'email'         => '//div[@class="adbox"]/following-sibling::table[1]/tr[5]/td[3]/a',
                'web'           => '//div[@class="adbox"]/following-sibling::table[1]/tr[6]/td[3]/a',
                'street'        => '//div[@class="adbox"]/following-sibling::table[1]/tr[5]/td[1]',
                'zipCity'       => '//div[@class="adbox"]/following-sibling::table[1]/tr[6]/td[1]',
                'mayor_name'    => '//div[@class="adbox"]/following-sibling::table[2]/tr[8]/td[2]'
            ];

            foreach ($mapDataXpath as $dataKey => $xpathQuery) {
                $element = $xpath->query($xpathQuery);
                if ( $element !== false && $element->item(0) != NULL ) {
                    $cityDetailData[$dataKey] = $element->item(0)->textContent;
                } else {
                    $cityDetailData[$dataKey] = "";
                }
            }

            $cityDetailData['city_hall_address'] = $cityDetailData['street'] . ", " . $cityDetailData['zipCity'];
            unset($cityDetailData['street']);
            unset($cityDetailData['zipCity']);

            $element = $xpath->query('//div[@class="adbox"]/following-sibling::table[1]/tr/td/img');
            if ( $element !== false
                && $element->item(0) != NULL
                && $element->item(0)->attributes != NULL
                && $element->item(0)->attributes->getNamedItem('src') != NULL
            ) {
                $imageUrl = $element->item(0)->attributes->getNamedItem('src')->textContent;
                $imageFileName = '/images/cities/' . substr($imageUrl, strrpos($imageUrl, '/') + 1);

                if (!file_exists(public_path() . $imageFileName)) {
                    file_put_contents(
                        public_path() . $imageFileName,
                        file_get_contents($imageUrl)
                    );
                }

                $cityDetailData['image_url'] = $imageFileName;
            } else {
                $cityDetailData['image_url'] = "";
            }

        } catch(Exception $e) {
            throw $e;
        }

        return $cityDetailData;
    }

    /**
     * Tato funkcia zo zadanej url stiahne obsah stranky vo formate html,
     * vytvori DOMDocument do ktoreho loadne stiahnute html,
     * nasledne z DOMdocumentu vytvori xpath objekt ktory returnuje
     *
     * @param string $url - url stranky na stiahnutie
     * @return DOMXpath - xpath objekt pre danu url
     */
    private function getXpathDoc(string $url): DOMXpath
    {

        $validator = Validator::make(["url" => $url], [
            'url'    => 'required|url',
        ]);

        if ($validator->fails()) {
            throw new Exception("BAD URL: " . $url);
        }

        $contents = file_get_contents($url);
        $doc = new DOMDocument();
        $doc->loadHTML($contents, LIBXML_NOWARNING | LIBXML_NOERROR);

        $xpath = new DOMXpath($doc);

        return $xpath;
    }

}