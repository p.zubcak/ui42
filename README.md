##INSTALLATION AND USAGE INSTRUCTIONS


clone from github,

    git clone https://gitlab.com/p.zubcak/ui42.git myapp

go to myapp directory

    cd myapp

update dependencies over composer

    composer update

install dependencies over npm

    npm install

now create empty db with utf8mb4 encoding

create env file

    cp .env.example .env
    php artisan key:generate

edit env

    set db connection

        DB_HOST=127.0.0.1
        DB_PORT=3306
        DB_DATABASE=laravel
        DB_USERNAME=root
        DB_PASSWORD=

    add google_api_key

        GOOGLE_API_KEY=your_google_api_key

run migrations

    php artisan migrate

compile css and javascript

    npm run dev
        or
    npm run prod

myapp is now alive http://127.0.0.1/myapp/public/

for import cities execute

    php artisan data:import

for geolocate cities execute

    php artisan data:geocode
